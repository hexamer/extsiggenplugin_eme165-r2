#pragma once

#include "ExtSigGen.h"
#include <Windows.h>
#include <string>
#include <unordered_map>

class EME165R2 : public ExtSigGen
{
    public:
        EXTSIGGENAPI_API EME165R2();
        EXTSIGGENAPI_API ~EME165R2();

        EXTSIGGENAPI_API bool Initialize(const wchar_t* _initString);
        EXTSIGGENAPI_API bool Terminate(void);
        EXTSIGGENAPI_API bool Ready(void);
        EXTSIGGENAPI_API bool SetSignalGenerator(double amplitudeVpp, double offsetV, double frequencyHz);
        EXTSIGGENAPI_API bool DisableSignalGenerator(void);
        EXTSIGGENAPI_API bool GetNearestFrequency(double frequency, double* nearestFrequency);
        EXTSIGGENAPI_API bool GetMaxFrequency(double* maxFrequency);
        EXTSIGGENAPI_API bool GetMinFrequency(double* minFrequency);
        EXTSIGGENAPI_API bool GetMaxAmplitudeVpp(double* maxAmplitudeVpp, double minFrequency, double maxFrequency);
        EXTSIGGENAPI_API bool GetMinAmplitudeVpp(double* minAmplitudeVpp, double minFrequency, double maxFrequency);
        EXTSIGGENAPI_API bool IsDDS(double* dacFrequency, uint8_t* phaseAccumulatorSize);
        EXTSIGGENAPI_API bool SupportsDcOffset(void);

    private:
        typedef struct
        {
            HANDLE hComm;
            LPOVERLAPPED pReadOverlapped;
            LPOVERLAPPED pWaitOverlapped;
            std::unordered_map<int, HANDLE>* pEvents;
            std::unordered_map<int, std::string>* pResponses;
        } ThreadParams;

        bool bInitialized;
        const DWORD SIG_GEN_TIMEOUT = 5000; // 5 seconds
        std::unordered_map<int, HANDLE> events;
        std::unordered_map<int, std::string> responses;
        HANDLE hComm = NULL;
        HANDLE hReadSerialThread = NULL;
        OVERLAPPED osReader;
        OVERLAPPED osWaiter;
        OVERLAPPED osWriter;
        std::wstring initString;
        ThreadParams threadParams;

        void InitSerial( void );
        void TerminateSerial( void );
        static DWORD WINAPI ReadSerial(LPVOID lpdwThreadParam);
        void WriteSerial(std::string data);
};