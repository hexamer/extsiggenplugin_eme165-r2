//////////////////////////////////////////////////////////////////////////////////////////////////
//
// EME165-R2 Plugin for Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module EME165R2.cpp: Implementation for the plugin library to provide an external signal
//                      generator for FRA4PicoScope based on the Minikits EME165R2.
//
// Notes: This is a plugin supporting an external signal generator for the Fra4PicoScope
//        application.  This project builds a DLL (the plugin) intended to reside in the same
//        directory as Fra4PicoScope.exe.  This plugin supports a custom signal generator based
//        on a Linduino and Mini-Kits EME165-R2 DDS board.  The Arduino code supports a
//        command/response interface using CmdMessenger.  So, most of the code in this plugin is
//        to support communication through that interface.  While this library could have used
//        the C# library for CmdMessenger, it seemed simpler to just handle the communications
//        directly, rather than deal with invoking a C# DLL from C++.
//
//        It is assumed that any function communicating with a signal generator will timeout in
//        a reasonable timeframe so that the FRA4PicoScope application doesn't hang indefinitely
//        in the event signal generator communications fail.
//
//        Since it is the first plugin made for this external signal generator feature it serves
//        as an example for other plugins for other signal generators.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "EME165R2.h"
#include <exception>
#include <locale>
#include <codecvt>
#include <sstream>

#define VALID_HANDLE(h) (h != INVALID_HANDLE_VALUE && h != (HANDLE)NULL)
#define INVALID_HANDLE(h) (h == INVALID_HANDLE_VALUE || h == (HANDLE)NULL)

//  The commands and responses supported by the Linduino
enum
{
    rspStatus,                       // 0 Readiness status
    cmdSetFrequency,                 // 1 Command to set DDS frequency
    rspSetFrequency,                 // 2 Response to set DDS frequency
    cmdSetVppAmplitude,              // 3 Command to set DDS amplitude
    rspSetVppAmplitude,              // 4 Response to set DDS amplitude
    cmdSetFrequencyAndVppAmplitude,  // 5 Command to simultaneously set DDS frequency and amplitude
    rspSetFrequencyAndVppAmplitude,  // 6 Response to simultaneously set DDS frequency and amplitude
    cmdGetNearestFrequency,          // 7 Command to get the frequency that will actually be set
    rspNearestFrequency,             // 8 Frequency that will actually be set
    cmdGetMaxFrequency,              // 9 Get Max Frequency
    rspMaxFrequency,                 // 10 Max Frequency
    cmdGetMinFrequency,              // 11 Get Min Frequency
    rspMinFrequency,                 // 12 Min Frequency
    cmdGetMaxVppAmplitude,           // 13 Get Max Peak to Peak Amplitude
    rspMaxVppAmplitude,              // 14 Max Peak to Peak Amplitude
    cmdGetMinVppAmplitude,           // 15 Get Min Peak to Peak Amplitude
    rspMinVppAmplitude,              // 16 Max Peak to Peak Amplitude
    cmdGetStatus,                    // 17 Gets the readiness status
    cmdReinitialize = 20             // 20 Reset AD9951 and initialize its registers
};

// Static singleton object for the signal generator to be used in this simple plugin
EME165R2 eme165r2;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: OpenExtSigGen
//
// Purpose: Opens a signal generator and supplies a new signal generator object to access/control it
//
// Parameters: [in] id - string representing a unique ID for the signal generator.  May have been
//                       returned from the EnumerateExtSigGen function
//             [out] return - pointer to the new signal generator
//
// Notes: Returned signal generator should be closed by the calling application as part of cleanup
//        using the CloseExtSigGen function.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" EXTSIGGENAPI_API ExtSigGen * OpenExtSigGen( wchar_t* id )
{
    UNREFERENCED_PARAMETER(id);
    return &eme165r2;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CloseExtSigGen
//
// Purpose: Closes (returns) a signal generator object that was created with OpenExtSigGen
//
// Parameters: [in] pESG - pointer to the external signal generator object to close
//             [out] - N/A
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" EXTSIGGENAPI_API void CloseExtSigGen( ExtSigGen * pESG )
{
    if (NULL!= pESG)
    {
        pESG -> Terminate();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EnumerateExtSigGen
//
// Purpose: Provides a list of signal generators available through the plugin
//
// Parameters: [out] count - the number of signal generators found
//             [out] ids - comma separated list of ids of signal generators found; memory must be
//                         supplied by the caller
//             [in/out] idLth - caller to set this to the available length of ids string; on return
//                              it's the length of the ids string used to store ids
//             [out] return - indicates whether the drivers supports enumeration
//
// Notes: Enumeration is optional; Function signature is modeled off PicoScope enumration
//
///////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" EXTSIGGENAPI_API bool EnumerateExtSigGen(uint16_t* count, wchar_t* ids, uint16_t* idLth)
{
#if 1
    return false; // This driver doesn't support enumeration.
#else // For test
    *count = 6;
    wsprintf( ids, L"18,23,36,21,9,99" );
    *idLth = 16;
    return true;
#endif
}

class SigGenFault : public std::exception {};

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::EME165R2
//
// Purpose: Simple constructor for the EME165R2 class
//
// Parameters: N/A
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

EME165R2::EME165R2()
{
    bInitialized = false;
    threadParams = {0};
    osReader = {0};
    osWaiter = {0};
    osWriter = {0};
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::EME165R2
//
// Purpose: Simple destructor for the EME165R2 class
//
// Parameters: N/A
//
// Notes: Calls Terminate in case the user forgets to
//
///////////////////////////////////////////////////////////////////////////////////////////////////

EME165R2::~EME165R2()
{
    Terminate();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::Initialize
//
// Purpose: Initialize the EME165R2 object
//
// Parameters: [in] _initString - A string containing initialization parameters/commands.  For this
//                                plugin, it identifies the COM port where the signal generator 
//                                resides.  E.g. COM4
//             [out] return - whether the initialization was successful
//
// Notes: Must be called before the object can be used
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::Initialize(const wchar_t* _initString)
{
    initString = _initString;
    DWORD dwWaitResult;
    int rspCode, rspData;

    // Initialize events if they haven't aleady been initialized
    // Start the receiving thread if it's not already started
    if (!bInitialized)
    {
        try
        {
            HANDLE event;
            event = CreateEventW( NULL, false, false, L"rspStatus Event" );
            events[rspStatus] = event;

            event = CreateEventW( NULL, false, false, L"rspSetFrequencyAndVppAmplitude Event" );
            events[rspSetFrequencyAndVppAmplitude] = event;

            event = CreateEventW( NULL, false, false, L"rspSetVppAmplitude Event" );
            events[rspSetVppAmplitude] = event;

            event = CreateEventW( NULL, false, false, L"rspNearestFrequency Event" );
            events[rspNearestFrequency] = event;
        
            event = CreateEventW( NULL, false, false, L"rspMaxFrequency Event" );
            events[rspMaxFrequency] = event;

            event = CreateEventW( NULL, false, false, L"rspMinFrequency Event" );
            events[rspMinFrequency] = event;

            event = CreateEventW( NULL, false, false, L"rspMaxVppAmplitude Event" );
            events[rspMaxVppAmplitude] = event;

            event = CreateEventW( NULL, false, false, L"rspMinVppAmplitude Event" );
            events[rspMinVppAmplitude] = event;

            for (std::pair<int, HANDLE> element : events)
            {
                if (INVALID_HANDLE(element.second))
                {
                    bInitialized = false;
                    throw SigGenFault();
                }
            }

            InitSerial();

            threadParams.hComm = hComm;
            threadParams.pReadOverlapped = &osReader;
            threadParams.pWaitOverlapped = &osWaiter;
            threadParams.pEvents = &events;
            threadParams.pResponses = &responses;
            hReadSerialThread = CreateThread(NULL, 0, ReadSerial, &threadParams, 0, NULL);
            if (INVALID_HANDLE(hReadSerialThread))
            {
                throw SigGenFault();
            }

            std::stringstream initCommand;
            initCommand << (int)cmdReinitialize << ";";
            WriteSerial(initCommand.str());

            dwWaitResult = WaitForSingleObject( events[rspStatus], SIG_GEN_TIMEOUT );
            if (WAIT_OBJECT_0 == dwWaitResult)
            {
                sscanf_s(responses[rspStatus].c_str(), "%d,%d", &rspCode, &rspData);
                if (rspData == 0)
                {
                    bInitialized = true;
                }
                else
                {
                    throw SigGenFault();
                }
            }
            else
            {
                throw SigGenFault();
            }
        }
        catch (const SigGenFault& e)
        {
            UNREFERENCED_PARAMETER(e);
            for (std::pair<int, HANDLE> element : events)
            {
                if (VALID_HANDLE(element.second))
                {
                    CloseHandle(element.second);
                }
            }
            if (VALID_HANDLE(hReadSerialThread))
            {
                CloseHandle(hReadSerialThread);
            }
            bInitialized = false;
        }
    }
    else // allows subsequent calls to Initialize without calling terminate
    {
        Terminate();
        Initialize(_initString);
    }

    return bInitialized;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::Terminate
//
// Purpose: Finish use of the EME165R2 object
//
// Parameters: [out] return - whether the call was successful
//
// Notes: Should be called before the object is destroyed
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::Terminate(void)
{
    if (bInitialized)
    {
        TerminateSerial();

        if (VALID_HANDLE(hReadSerialThread))
        {
            CloseHandle(hReadSerialThread);
            hReadSerialThread = (HANDLE)NULL;
        }

        for (std::pair<int, HANDLE> element : events)
        {
            if (VALID_HANDLE(element.second))
            {
                CloseHandle(element.second);
                element.second = (HANDLE(NULL));
            }
        }

        bInitialized = false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::SetSignalGenerator
//
// Purpose: Command the signal generator to output a sine signal with the specified parameters
//
// Parameters: [in] amplitudeVpp - Amplitude Volts, peak to peak
//             [in] offsetV - Offset in Volts
//             [in] frequencyHz - Frequency in Hertz
//             [out] return - whether the call was successful
//
// Notes: Offset is not supported by this plugin, so is ignored
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::SetSignalGenerator(double amplitudeVpp, double offsetV, double frequencyHz)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode, rspData;

    UNREFERENCED_PARAMETER(offsetV);

    if (bInitialized)
    {
        try
        {
            if( !ResetEvent( events[rspSetFrequencyAndVppAmplitude] ) )
            {
                throw SigGenFault();
            }

            message << (int)cmdSetFrequencyAndVppAmplitude << "," << frequencyHz << "," << amplitudeVpp << ";";
            WriteSerial(message.str());

            dwWaitResult = WaitForSingleObject( events[rspSetFrequencyAndVppAmplitude], SIG_GEN_TIMEOUT );

            if (WAIT_OBJECT_0 == dwWaitResult)
            {
                sscanf_s(responses[rspSetFrequencyAndVppAmplitude].c_str(), "%d,%d", &rspCode, &rspData);
                if (rspData == 0)
                {
                    retval = true;
                }
                else
                {
                    retval = false;
                }
            }
            else
            {
                retval = false;
            }
        }
        catch (const SigGenFault& e)
        {
            UNREFERENCED_PARAMETER(e);
            retval = false;
        }
        return retval;
    }
    else
    {
        return retval;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::DisableSignalGenerator
//
// Purpose: Command the signal generator to stop output
//
// Parameters: [out] return - whether the call was successful
//
// Notes: This method would ideally set the output to high impedance.  But since I haven't
//        desiged the custom DDS to support that yet it currently just sets the amplitude to 0
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::DisableSignalGenerator(void)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode, rspData;

    try
    {
        if( !ResetEvent( events[rspSetVppAmplitude] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdSetVppAmplitude << ", 0.0;";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspSetVppAmplitude], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspSetVppAmplitude].c_str(), "%d,%d", &rspCode, &rspData);
            if (rspData == 0)
            {
                retval = true;
            }
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::GetNearestFrequency
//
// Purpose: Get the frequency closest to the desired frequency that the signal generator is capable
//          of generating.
//
// Parameters: [in] frequency - desired frequency in Hertz
//             [out] nearestFrequency - nearest frequency in Hertz
//             [out] return - whether the call was successful
//
// Notes: DDS has limited resoution.  This function tells us the actual frequency the signal
//        signal generator will generate so that the FRA application can tune the DDS accordingly.
//        If the signal generator technology doesn't have a frequency quantization of any practical
//        importance, then this function can just return the same parameter passed to it.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::GetNearestFrequency(double frequency, double* nearestFrequency)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode;
    double minFrequency, maxFrequency;
    double _nearestFrequency;

    try
    {
        if( !ResetEvent( events[rspNearestFrequency] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdGetNearestFrequency << "," << frequency << ";";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspNearestFrequency], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspNearestFrequency].c_str(), "%d,%lf", &rspCode, &_nearestFrequency );

            if (GetMinFrequency( &minFrequency ))
            {
                if (GetMaxFrequency( &maxFrequency ))
                {
                    _nearestFrequency = min( _nearestFrequency, maxFrequency );
                    _nearestFrequency = max( _nearestFrequency, minFrequency );

                    *nearestFrequency = _nearestFrequency;
                    retval = true;
                }
            }
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::GetMaxFrequency
//
// Purpose: Get the maximum frequency the signal generator is capable of generating.
//
// Parameters: [out] maxFrequency - maximum frequency in Hertz
//             [out] return - whether the call was successful
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::GetMaxFrequency(double* maxFrequency)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode;

    try
    {
        if( !ResetEvent( events[rspMaxFrequency] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdGetMaxFrequency << ";";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspMaxFrequency], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspMaxFrequency].c_str(), "%d,%lf", &rspCode, maxFrequency);
            retval = true;
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::GetMinFrequency
//
// Purpose: Get the minimum frequency the signal generator is capable of generating.
//
// Parameters: [out] minFrequency - minimum frequency in Hertz
//             [out] return - whether the call was successful
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::GetMinFrequency(double* minFrequency)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode;

    try
    {
        if( !ResetEvent( events[rspMinFrequency] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdGetMinFrequency << ";";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspMinFrequency], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspMinFrequency].c_str(), "%d,%lf", &rspCode, minFrequency);
            retval = true;
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::GetMaxAmplitudeVpp
//
// Purpose: Get the maximum amplitude the signal generator is capable of generating.
//
// Parameters: [out] maxAmplitudeVpp - maximum amplitude in Volts, peak to peak
//             [out] return - whether the call was successful
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::GetMaxAmplitudeVpp(double* maxAmplitudeVpp, double minFrequency, double maxFrequency)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode;

    try
    {
        if( !ResetEvent( events[rspMaxVppAmplitude] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdGetMaxVppAmplitude << ";";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspMaxVppAmplitude], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspMaxVppAmplitude].c_str(), "%d,%lf", &rspCode, maxAmplitudeVpp);
            retval = true;
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::GetMinAmplitudeVpp
//
// Purpose: Get the minimum amplitude the signal generator is capable of generating.
//
// Parameters: [out] minAmplitudeVpp - minimum amplitude in Volts, peak to peak
//             [out] return - whether the call was successful
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::GetMinAmplitudeVpp(double* minAmplitudeVpp, double minFrequency, double maxFrequency)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode;

    try
    {
        if( !ResetEvent( events[rspMinVppAmplitude] ) )
        {
            throw SigGenFault();
        }

        message << (int)cmdGetMinVppAmplitude << ";";
        WriteSerial(message.str());

        dwWaitResult = WaitForSingleObject( events[rspMinVppAmplitude], SIG_GEN_TIMEOUT );

        if (WAIT_OBJECT_0 == dwWaitResult)
        {
            sscanf_s(responses[rspMinVppAmplitude].c_str(), "%d,%lf", &rspCode, minAmplitudeVpp);
            retval = true;
        }
        else
        {
            retval = false;
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        retval = false;
    }
    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::Ready
//
// Purpose: Indicates whether the signal generator is ready to be used
//
// Parameters: [out] return - whether the signal generator is ready to be used
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::Ready(void)
{
    bool retval = false;
    std::stringstream message;
    DWORD dwWaitResult;
    int rspCode, rspData;

    if (bInitialized)
    {
        try
        {
            if( !ResetEvent( events[rspStatus] ) )
            {
                throw SigGenFault();
            }

            message << (int)cmdGetStatus << ";";
            WriteSerial(message.str());

            dwWaitResult = WaitForSingleObject( events[rspStatus], SIG_GEN_TIMEOUT );

            if (WAIT_OBJECT_0 == dwWaitResult)
            {
                sscanf_s(responses[rspStatus].c_str(), "%d,%d", &rspCode, &rspData);
                if (0 == rspData)
                {
                    retval = true;
                }
                else
                {
                    retval = false;
                }
            }
            else
            {
                retval = false;
            }
        }
        catch (const SigGenFault& e)
        {
            UNREFERENCED_PARAMETER(e);
            retval = false;
        }
    }
    else
    {
        retval = false;
    }

    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::IsDDS
//
// Purpose: Indicates whether the signal generator is a DDS.
//
// Parameters: [out] dacFrequency - DDS DAC frequency in samples/second
//             [out] phaseAccumulatorSize - DDS phase accumulator size in bits
//             [out] return - whether the signal generator is a DDS
//
// Notes: Not all signal generators are DDS.  It could be a VCO or anything else capable of
//        generating a sine wave.  If the return value is false, the other out parameters are
//        invalid.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::IsDDS(double* dacFrequency, uint8_t* phaseAccumulatorSize)
{
    *dacFrequency = 400e6;
    *phaseAccumulatorSize = 32;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::SupportsDcOffset
//
// Purpose: Indicates whether the signal generator supports a DC offset.
//
// Parameters: [out] return - whether the signal generator supports a DC offset
//
// Notes: Since it's not a general purpose signal generator, the EME165-R2 does not support a
//        dc offset
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EME165R2::SupportsDcOffset(void)
{
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EME165R2::NNNSerial
//
// Purpose: Private functions supporting initialization, termination, reading and writing to the
//          serial port to support CmdMessenger transactions.
//
// Parameters: various
//
// Notes: These use overlapped I/O with timeouts, which is important to avoid deadlock
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void EME165R2::InitSerial( void )
{
    try
    {
        std::wstringstream fullSerialPortFilename;
        fullSerialPortFilename << L"\\\\.\\" << initString;

        hComm = CreateFile( fullSerialPortFilename.str().c_str(),  // port name
                            GENERIC_READ | GENERIC_WRITE,          // Read/Write
                            0,                                     // No Sharing
                            NULL,                                  // No Security
                            OPEN_EXISTING,                         // Open existing port only
                            FILE_FLAG_OVERLAPPED,                  // Overlapped I/O
                            NULL );                                // Null for Comm Devices
        if (INVALID_HANDLE(hComm))
        {
            throw SigGenFault();
        }

        osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (INVALID_HANDLE(osReader.hEvent))
        {
            throw SigGenFault();
        }

        osWaiter.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (INVALID_HANDLE(osWaiter.hEvent))
        {
            throw SigGenFault();
        }

        osWriter.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (INVALID_HANDLE(osWriter.hEvent))
        {
            throw SigGenFault();
        }

        DCB dcbSerialParams = { 0 }; // Initializing DCB structure
        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

        BOOL status = GetCommState(hComm, &dcbSerialParams);
        if (!status)
        {
            throw SigGenFault();
        }

        dcbSerialParams.BaudRate = CBR_115200;  // Setting BaudRate = 115200
        dcbSerialParams.ByteSize = 8;           // Setting ByteSize = 8
        dcbSerialParams.StopBits = ONESTOPBIT;  // Setting StopBits = 1
        dcbSerialParams.Parity   = NOPARITY;    // Setting Parity = None
        dcbSerialParams.EvtChar = '\n';         // Signal event when the Arduino CmdMessenger
                                                // new line is received

        status = SetCommState(hComm, &dcbSerialParams);
        if (!status)
        {
            throw SigGenFault();
        }

        COMMTIMEOUTS timeouts = { 0 };
        timeouts.ReadIntervalTimeout         = 50; // in milliseconds
        timeouts.ReadTotalTimeoutConstant    = 50; // in milliseconds
        timeouts.ReadTotalTimeoutMultiplier  = 10; // in milliseconds
        timeouts.WriteTotalTimeoutConstant   = 50; // in milliseconds
        timeouts.WriteTotalTimeoutMultiplier = 10; // in milliseconds

        status = SetCommTimeouts( hComm, &timeouts );

        if (!status)
        {
            throw SigGenFault();
        }

        status = SetCommMask( hComm, EV_RXFLAG ); // Signal event when the Arduino CmdMessenger
                                                  // new line is received

        if (!status)
        {
            throw SigGenFault();
        }
    }
    catch (const SigGenFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        if (VALID_HANDLE(hComm))
        {
            CloseHandle(hComm);
        }
        if (VALID_HANDLE(osReader.hEvent))
        {
            CloseHandle(osReader.hEvent);
        }
        if (VALID_HANDLE(osWaiter.hEvent))
        {
            CloseHandle(osWaiter.hEvent);
        }
        if (VALID_HANDLE(osWriter.hEvent))
        {
            CloseHandle(osWriter.hEvent);
        }
        throw SigGenFault();
    }
}

void EME165R2::TerminateSerial(void)
{
    DWORD dwWaitRes = 0;
    // Terminate the Read thread. Documentation says calling SetCommMask
    // will signal WaitCommEvent with lpEvtMask contents set to 0.
    // We need to wait with timeout because its possible the thread
    // is hung.  One likely case of this is where someone forgot
    // to call Terminate before unloading the DLL. If TermimateSerial
    // executes via the destructor while the DLL is being unloaded, the 
    // thread will hang at the return due to loader lock.
    SetCommMask(hComm, EV_RXFLAG);
    dwWaitRes= WaitForSingleObject( hReadSerialThread, 3000 );
    if (dwWaitRes == WAIT_TIMEOUT)
    {
        TerminateThread(hReadSerialThread, 0);
    }

    if (VALID_HANDLE(hComm))
    {
        CloseHandle(hComm);
    }
    if (VALID_HANDLE(osReader.hEvent))
    {
        CloseHandle(osReader.hEvent);
    }
    if (VALID_HANDLE(osWaiter.hEvent))
    {
        CloseHandle(osWaiter.hEvent);
    }
    if (VALID_HANDLE(osWriter.hEvent))
    {
        CloseHandle(osWriter.hEvent);
    }
}

DWORD WINAPI EME165R2::ReadSerial(LPVOID lpdwThreadParam)
{
    ThreadParams* pThreadParams;
    HANDLE hComm = NULL;
    LPOVERLAPPED pReadOverlapped = NULL;
    LPOVERLAPPED pWaitOverlapped = NULL;
    std::unordered_map<int, HANDLE>* pEvents;
    std::unordered_map<int, std::string>* pResponses;

    BOOL status;
    DWORD dwEventMask = 0;
    DWORD dwWaitRes = 0;
    char data;
    char serialBuffer[256]; //Buffer for storing Received Data
    DWORD NoBytesRead;
    int i = 0;
    int cmdCode;

    pThreadParams = (ThreadParams*)lpdwThreadParam;
    hComm = pThreadParams->hComm;
    pReadOverlapped = pThreadParams->pReadOverlapped;
    pWaitOverlapped = pThreadParams->pWaitOverlapped;
    pEvents = pThreadParams->pEvents;
    pResponses = pThreadParams->pResponses;

    for (;;)
    {
        try
        {
            status = WaitCommEvent(hComm, &dwEventMask, pWaitOverlapped);
            if (!status)
            {
                if (GetLastError() == ERROR_IO_PENDING)
                {
                    DWORD dwDummy = 0;
                    if (!GetOverlappedResult(hComm, pWaitOverlapped, &dwDummy, TRUE))
                    {
                        throw SigGenFault();
                    }
                    else
                    {
                        status = true;
                    }
                }
            }
            if (status && (dwEventMask&EV_RXFLAG))
            {
                i = 0;
                do
                {   // Read one byte at a time
                    if (!ReadFile( hComm,           // Handle of the Serial port
                                   &data,           // data character
                                   sizeof(data),    // Number of bytes to read
                                   &NoBytesRead,    // Number of bytes read
                                   pReadOverlapped ))
                    {
                        if (GetLastError() == ERROR_IO_PENDING)
                        {
                            if (!GetOverlappedResult(hComm, pReadOverlapped, &NoBytesRead, TRUE))
                            {
                                throw SigGenFault();
                            }
                        }
                        else
                        {
                            throw SigGenFault();
                        }
                    }
                    serialBuffer[i] = data;    // Store data into buffer
                    i++;
                } while (NoBytesRead > 0 && data != '\n' && i < sizeof(serialBuffer) - 1);
                serialBuffer[i] = 0; // null terminate the string before any processing

                sscanf_s( serialBuffer, "%d,", &cmdCode );

                // Store the results, then signal the received event
                if (pEvents->find(cmdCode) != pEvents->end() && (*pEvents)[cmdCode] != (HANDLE)NULL)
                {
                    (*pResponses)[cmdCode] = serialBuffer;
                    SetEvent((*pEvents)[cmdCode]);
                }
            }
            if (dwEventMask == 0) // We're being signalled to terminate
            {
                break;
            }
        }
        catch (const SigGenFault& e)
        {
            UNREFERENCED_PARAMETER(e);
            // Try to recover
            PurgeComm( hComm, PURGE_RXABORT | PURGE_RXCLEAR );
            continue;
        }
    }

    return 0;
}

void EME165R2::WriteSerial(std::string data)
{
    DWORD dNoOFBytestoWrite;                 // No of bytes to write into the port
    DWORD dNoOfBytesWritten = 0;             // No of bytes written to the port
    DWORD dwWaitRes = 0;
    DWORD dwErrors = 0;
    COMSTAT cs;
    dNoOFBytestoWrite = (DWORD)data.length();

    ClearCommError( hComm, &dwErrors, &cs);

    if (!WriteFile( hComm,               // Handle to the Serial port
                    data.c_str(),        // Data to be written to the port
                    dNoOFBytestoWrite,   // No of bytes to write
                    &dNoOfBytesWritten,  // Bytes written
                    &osWriter ))
    {
        if (GetLastError() == ERROR_IO_PENDING)
        {
            if (!GetOverlappedResult(hComm, &osWriter, &dNoOfBytesWritten, TRUE))
            {
                throw SigGenFault();
            }
        }
        else
        {
            throw SigGenFault();
        }
    }
}